package com.lab1886.mysoln.service;

import com.lab1886.mysoln.api.dto.UploadFileResponse;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface ImageStorageService {
    UploadFileResponse storeFile(MultipartFile file, String username, String hashtag) throws IllegalAccessException;

    Resource loadFileAsResource(String fileName, String username) throws IllegalAccessException;

}
