package com.lab1886.mysoln.service.impl;

import com.lab1886.mysoln.api.dto.UserDto;
import com.lab1886.mysoln.persistance.Role;
import com.lab1886.mysoln.persistance.User;
import com.lab1886.mysoln.persistance.repository.RoleRepository;
import com.lab1886.mysoln.persistance.repository.UserRepository;
import com.lab1886.mysoln.service.RegistrationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import static com.lab1886.mysoln.persistance.RoleTypes.AD;
import static com.lab1886.mysoln.persistance.RoleTypes.PSU;

@Service
public class RegistrationServiceImpl implements RegistrationService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDto save(UserDto userDto) {
        Role role = roleRepository.findRoleByRoleName(PSU)
                .orElseThrow(() -> new RuntimeException("Role missing for user"));
        User user = new User();
        user.setUsername(userDto.getUsername());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
        user.getRoles().add(role);
        userRepository.save(user);
        return userDto;
    }

    @Override
    public UserDto saveAdmin(UserDto userDto) {
        Role role = roleRepository.findRoleByRoleName(AD)
                .orElseThrow(() -> new RuntimeException("Role not found for admin"));
        User user = new User();
        user.setUsername(userDto.getUsername());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
        user.getRoles().add(role);
        userRepository.save(user);
        return userDto;
    }

    @Override
    public void delete(String username) {
        User user = userRepository.findByUsername(username);
        userRepository.delete(user);
    }
}
