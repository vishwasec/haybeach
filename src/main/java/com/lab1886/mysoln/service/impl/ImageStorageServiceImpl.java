package com.lab1886.mysoln.service.impl;

import com.lab1886.mysoln.api.dto.UploadFileResponse;
import com.lab1886.mysoln.config.FileStorageProperties;
import com.lab1886.mysoln.persistance.ImageDetails;
import com.lab1886.mysoln.persistance.User;
import com.lab1886.mysoln.persistance.repository.ImageRepository;
import com.lab1886.mysoln.persistance.repository.UserRepository;
import com.lab1886.mysoln.service.ImageStorageService;
import com.lab1886.mysoln.util.CacheType;
import com.lab1886.mysoln.util.CacheUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Optional;
import java.util.UUID;

@Service
public class ImageStorageServiceImpl implements ImageStorageService {

    private final Path fileStorageLocation;
    @Autowired
    private ImageRepository imageRepository;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    public ImageStorageServiceImpl(FileStorageProperties fileStorageProperties) {

        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();
        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new RuntimeException("Could not create the directory.", ex);
        }
    }

    @Transactional
    public UploadFileResponse storeFile(MultipartFile file, String username, String hashtag) throws IllegalAccessException {
        if (username == null) {
            throw new IllegalAccessException("Not allowed to upload image without token.");
        }

        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        Optional<ImageDetails> imageDetails = imageRepository
                .findImageDetailsByFileNameAndUser_Username(filename, username);
        User user = getUser(username);
        String savingname = imageDetails.isPresent() ? imageDetails.get().getSavedfileName()
                : UUID.randomUUID() + "_" + filename;

        try {
            String fileDownloadUri = saveFileByOverwrite(file, filename, savingname);

            persistDetailsToDb(file, filename, imageDetails, user, savingname, fileDownloadUri, hashtag);

            return UploadFileResponse.builder()
                    .fileName(filename).fileDownloadUri(fileDownloadUri)
                    .fileType(file.getContentType())
                    .size(file.getSize())
                    .build();

        } catch (IOException ex) {
            throw new RuntimeException("Could not store file " + filename + ". Please try again!", ex);
        }
    }

    private User getUser(String username) {
        User user = userRepository.findByUsername(username);
        if (user == null)
            throw new RuntimeException("User not allowed to upload/download images");
        return user;
    }

    private String saveFileByOverwrite(MultipartFile file, String filename, String savingname)
            throws IOException {
        Path targetLocation = this.fileStorageLocation.resolve(savingname);
        Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
        return ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/downloadFile/")
                .path(filename)
                .toUriString();
    }

    private void persistDetailsToDb(MultipartFile file, String filename
            , Optional<ImageDetails> imageDetails, User user, String savingname
            , String fileDownloadUri, String hashtag) {
        ImageDetails savingdetails = imageDetails.orElseGet(() -> {
            ImageDetails idetails = new ImageDetails();
            idetails.setFileDownloadUri(fileDownloadUri);
            idetails.setFileName(filename);
            idetails.setSavedfileName(savingname);
            idetails.setUser(user);
            idetails.setFileType(file.getContentType());
            idetails.setHashtag(hashtag);
            CacheUtil.getInstance(CacheType.HASHTAG)
                    .updateStringLongCacheForType(CacheType.HASHTAG, hashtag, 1l);
            return idetails;
        });

        imageRepository.save(savingdetails);
    }

    public Resource loadFileAsResource(String fileName, String username) throws IllegalAccessException {
        if (username == null) {
            throw new IllegalAccessException("Not allowed to upload image without token.");
        }

        try {
            ImageDetails imageDetails = imageRepository
                    .findImageDetailsByFileNameAndUser_Username(fileName, username)
                    .orElseThrow(() -> new RuntimeException("User not allowed to download."));
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new RuntimeException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new RuntimeException("File not found " + fileName, ex);
        }
    }

}
