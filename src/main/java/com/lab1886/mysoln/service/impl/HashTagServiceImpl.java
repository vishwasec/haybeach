package com.lab1886.mysoln.service.impl;

import com.lab1886.mysoln.persistance.ImageDetails;
import com.lab1886.mysoln.persistance.repository.ImageRepository;
import com.lab1886.mysoln.persistance.repository.UserRepository;
import com.lab1886.mysoln.service.HashTagService;
import com.lab1886.mysoln.util.CacheType;
import com.lab1886.mysoln.util.CacheUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class HashTagServiceImpl implements HashTagService {

    @Autowired
    private ImageRepository imageRepository;
    @Autowired
    private UserRepository userRepository;


    @Override
    public Set<String> getHashTagsFromCache() {
        return CacheUtil.getInstance(CacheType.HASHTAG)
                .getStringListforType(CacheType.HASHTAG);
    }

    @Override
    public Set<String> getHashTagsFromDB() {
        return imageRepository.findDistinctFirstByHashtag().stream()
                .map(ImageDetails::getHashtag).collect(Collectors.toSet());
    }

}
