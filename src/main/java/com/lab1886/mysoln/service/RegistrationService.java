package com.lab1886.mysoln.service;

import com.lab1886.mysoln.api.dto.UserDto;

public interface RegistrationService {
    UserDto save(UserDto userDto);

    UserDto saveAdmin(UserDto userDto);

    void delete(String username);
}
