package com.lab1886.mysoln.service;

import java.util.Set;

public interface HashTagService {
    Set<String> getHashTagsFromCache();

    Set<String> getHashTagsFromDB();
}
