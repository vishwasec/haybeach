package com.lab1886.mysoln;

import com.lab1886.mysoln.config.FileStorageProperties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({
        FileStorageProperties.class
})
public class MysolnApplication {

    public static void main(String[] args) {
        SpringApplication.run(MysolnApplication.class, args);
    }

}

