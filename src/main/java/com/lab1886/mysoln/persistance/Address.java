package com.lab1886.mysoln.persistance;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.Data;


@Entity
@Data
public class Address {
    private static final long serialVersionUID = 1L;
    @OneToOne()
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    User user;
    @OneToOne()
    @JoinColumn(name = "image_id", referencedColumnName = "id")
    ImageDetails imageDetails;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String deliveryAddress;
    @Column
    private String fullname;
}
