package com.lab1886.mysoln.persistance.repository;

import com.lab1886.mysoln.persistance.User;

import org.springframework.data.repository.CrudRepository;


public interface UserRepository extends CrudRepository<User, Long> {
    User findByUsername(String username);
}
