package com.lab1886.mysoln.persistance.repository;

import com.lab1886.mysoln.persistance.Role;
import com.lab1886.mysoln.persistance.RoleTypes;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;


public interface RoleRepository extends CrudRepository<Role, Long> {
    Optional<Role> findRoleByRoleName(RoleTypes roleTypes);
}
