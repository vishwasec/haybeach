package com.lab1886.mysoln.persistance.repository;

import com.lab1886.mysoln.persistance.ImageDetails;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;


public interface ImageRepository extends CrudRepository<ImageDetails, Long> {
    Optional<ImageDetails> findImageDetailsByFileNameAndUser_Username(String filename, String username);

    @Query(value = "select distinct hashtag,* from image", nativeQuery = true)
    List<ImageDetails> findDistinctFirstByHashtag();
}
