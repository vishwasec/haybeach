package com.lab1886.mysoln.persistance;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;


@Entity
@Table(name = "favorites")
@Data
public class FavoritePics {
    private static final long serialVersionUID = 1L;
    @OneToOne()
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    User user;
    @OneToOne()
    @JoinColumn(name = "image_id", referencedColumnName = "id")
    ImageDetails imageDetails;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
}
