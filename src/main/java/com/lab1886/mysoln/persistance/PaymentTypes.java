package com.lab1886.mysoln.persistance;

public enum PaymentTypes {
    CASH, CARD, WALLET
}
