package com.lab1886.mysoln.auth.exception;

public class TokenException extends RuntimeException {

    private static final long serialVersionUID = 8583654761978516523L;

    public TokenException(final String message) {
        super(message);
    }

    public TokenException(final String message, final Throwable th) {

        super(message, th);
    }
}