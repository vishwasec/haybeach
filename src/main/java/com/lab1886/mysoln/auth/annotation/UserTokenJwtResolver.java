package com.lab1886.mysoln.auth.annotation;

import com.lab1886.mysoln.auth.TokenManagement;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import java.io.IOException;
import java.util.Map;

public class UserTokenJwtResolver implements HandlerMethodArgumentResolver {

    private static final String USER_NAME = "user_name";

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterAnnotation(UserName.class) != null;
    }

    @Override
    public Object resolveArgument(MethodParameter parameter,
                                  ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest,
                                  WebDataBinderFactory binderFactory) throws IOException {

        Map<String, ?> accessTokenInfo = TokenManagement.getAccountFromToken(Map.class);
        return accessTokenInfo.get(USER_NAME);
    }

}