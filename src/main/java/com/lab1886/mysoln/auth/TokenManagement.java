package com.lab1886.mysoln.auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lab1886.mysoln.auth.exception.TokenException;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;

import java.io.IOException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TokenManagement {

    private static final ObjectMapper MAPPER = new ObjectMapper();


    private TokenManagement() {
    }

    public static <T> T getAccountFromToken(Class<T> returnType) throws IOException {
        OAuth2AuthenticationDetails tokenDetails = (OAuth2AuthenticationDetails)
                SecurityContextHolder.getContext().getAuthentication().getDetails();

        T accessTokenInfo = MAPPER.readValue(JwtHelper.decode(tokenDetails.getTokenValue()).getClaims(), returnType);
        if (accessTokenInfo == null) {
            throw new TokenException("Missing accessTokenInfo from auth server response");
        }

        return accessTokenInfo;
    }

}
