package com.lab1886.mysoln.api;

import com.lab1886.mysoln.service.HashTagService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@RestController
@Api(value = "haybeach", description = "Upload images with hastags.")
@Slf4j
public class HashTagController {
    @Autowired
    private HashTagService hashTagService;

    @GetMapping("/hashtag")
    @PreAuthorize("hasAuthority('PSU')")
    @ApiOperation(value = "Gets all hashtags from cache. Should be a PSU user.")
    public Set<String> hashtag() {
        return hashTagService.getHashTagsFromCache();
    }

    @GetMapping("/all/hashtag")
    @PreAuthorize("hasAuthority('PSU')")
    @ApiOperation(value = "Gets all hashtags from db. Should be a PSU user.")
    public Set<String> allhashtag(
    ) {
        return hashTagService.getHashTagsFromDB();
    }


}
