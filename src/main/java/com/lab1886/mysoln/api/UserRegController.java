package com.lab1886.mysoln.api;

import com.lab1886.mysoln.api.dto.UserDto;
import com.lab1886.mysoln.service.RegistrationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/user/registration")
@Api(value = "haybeach", description = "Register Admin and psu users.")
public class UserRegController {
    @Autowired
    private RegistrationService registrationService;

    @PostMapping()
    @ApiOperation(value = "Creates PSU users.")
    public UserDto saveUser(@RequestBody UserDto userDto) {
        return registrationService.save(userDto);
    }

    @PostMapping("/admin")
    @ApiOperation(value = "Creates Admin users. Should be a SYS.")
    @PreAuthorize("hasAuthority('SYS')")
    public UserDto saveAdminUser(@RequestBody UserDto userDto) {
        return registrationService.save(userDto);
    }

    @DeleteMapping()
    @PreAuthorize("hasAuthority('SYS') or hasAuthority('AD')")
    @ApiOperation(value = "Deletes users. Should be a SYS/AD.")
    public void delete(@RequestParam("username") String username) {
        registrationService.delete(username);
    }
}
