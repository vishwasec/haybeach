package com.lab1886.mysoln.api.dto;

import lombok.Data;

@Data
public class UserDto {

    private String username;

    private String password;

    private String firstName;

    private String lastName;

}

