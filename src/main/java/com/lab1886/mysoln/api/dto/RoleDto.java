package com.lab1886.mysoln.api.dto;

import lombok.Data;


@Data
public class RoleDto {
    private String roleName;

    private String description;

}
