package com.lab1886.mysoln.api;

import com.lab1886.mysoln.api.dto.UploadFileResponse;
import com.lab1886.mysoln.auth.annotation.UserName;
import com.lab1886.mysoln.service.ImageStorageService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@RestController
@Api(value = "haybeach", description = "Upload images with hastags.")
@Slf4j
public class UploadPicsController {
    @Autowired
    private ImageStorageService imageStorageService;

    @PostMapping("/uploadImage")
    @PreAuthorize("hasAuthority('PSU')")
    @ApiOperation(value = "Uploads images with hashtags. Should be a PSU user.")
    public UploadFileResponse uploadFile(@RequestParam("image") MultipartFile file
            , @RequestParam("hashtag") String hashtag,
                                         @UserName String username) throws IllegalAccessException {
        return imageStorageService.storeFile(file, username, hashtag);
    }

    @GetMapping("/downloadImage/{fileName:.+}")
    @PreAuthorize("hasAuthority('PSU')")
    @ApiOperation(value = "Downloads images with hashtags. Should be a PSU user.")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName
            , HttpServletRequest request, @UserName String username) throws IllegalAccessException {

        Resource resource = imageStorageService.loadFileAsResource(fileName, username);
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            log.info("Could not determine file type.");
        }
        if (contentType == null) {
            contentType = "application/octet-stream";
        }
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }


}
