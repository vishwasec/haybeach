package com.lab1886.mysoln.util;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public final class InMemCache<T, E> {

    private final Map<T, E> cache = new ConcurrentHashMap();

    InMemCache() {

    }

    public Collection<E> getAllValues() {
        return cache.values();
    }

    public Set<T> getKeys() {
        return cache.keySet();
    }

    public void put(T key, E value) {
        cache.put(key, value);
    }

    public E get(T key) {
        return cache.get(key);
    }
}
