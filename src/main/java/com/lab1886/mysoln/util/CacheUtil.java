package com.lab1886.mysoln.util;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public final class CacheUtil {
    private final static Map<CacheType, InMemCache> mycache = new ConcurrentHashMap<>();
    private static CacheUtil cacheUtil;

    private CacheUtil() {
    }

    public synchronized static CacheUtil getInstance(CacheType cacheType) {
        if (cacheUtil == null)
            cacheUtil = new CacheUtil();

        return cacheUtil;
    }

    private InMemCache<String, Long> getStringLongCacheForType(CacheType cacheType) {
        if (mycache.containsKey(cacheType))
            return mycache.get(cacheType);
        else {
            InMemCache<String, Long> inMemCache = new InMemCache<>();
            mycache.put(cacheType, inMemCache);
            return mycache.get(cacheType);
        }
    }

    public void updateStringLongCacheForType(CacheType cacheType, String key, Long value) {
        InMemCache<String, Long> inMemCache = getStringLongCacheForType(cacheType);
        Long count = inMemCache.get(key);

        if (count == null)
            count = 0l;

        count += value;
        inMemCache.put(key, count);
    }

    public Set<String> getStringListforType(CacheType cacheType) {
        InMemCache<String, Long> inMemCache = getStringLongCacheForType(cacheType);
        return inMemCache.getKeys();
    }

}
