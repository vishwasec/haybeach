package com.lab1886.mysoln.service.impl;

import com.lab1886.mysoln.api.dto.UserDto;
import com.lab1886.mysoln.persistance.Role;
import com.lab1886.mysoln.persistance.RoleTypes;
import com.lab1886.mysoln.persistance.User;
import com.lab1886.mysoln.persistance.repository.RoleRepository;
import com.lab1886.mysoln.persistance.repository.UserRepository;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)

public class RegistrationServiceImplTest {
    @InjectMocks
    RegistrationServiceImpl registrationService;
    @Mock
    UserRepository userRepository;
    @Mock
    RoleRepository roleRepository;
    @Mock
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Test
    public void save() {
        Role role = new Role();
        Mockito.when(roleRepository.findRoleByRoleName(RoleTypes.PSU)).thenReturn(java.util.Optional.ofNullable(role));
        UserDto userDto = getUserDto();
        UserDto returned = registrationService.save(userDto);
        Assert.assertNotNull(returned);
        Assert.assertEquals("vis", userDto.getFirstName());
    }

    private UserDto getUserDto() {
        UserDto userDto = new UserDto();
        userDto.setFirstName("vis");
        userDto.setLastName("was");
        userDto.setPassword("adfaf");
        userDto.setUsername("vgopala");
        return userDto;
    }

    @Test
    public void saveAdmin() {
        Role role = new Role();
        Mockito.when(roleRepository.findRoleByRoleName(RoleTypes.AD)).thenReturn(java.util.Optional.ofNullable(role));
        UserDto userDto = getUserDto();
        UserDto returned = registrationService.saveAdmin(userDto);
        Assert.assertNotNull(returned);
        Assert.assertEquals("vis", userDto.getFirstName());

    }

    @Test
    public void delete() {
        User user = new User();
        user.setUsername("vishwas");
        Mockito.when(userRepository.findByUsername("vishwas")).thenReturn(user);

        registrationService.delete("vishwas");
    }
}