package com.lab1886.mysoln.api;

import com.lab1886.mysoln.MysolnApplication;
import com.lab1886.mysoln.exception.BeachErrorHandler;
import com.lab1886.mysoln.service.HashTagService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(MysolnApplication.class)
public class HashTagControllerTest {
    private MockMvc mvc;

    @Mock
    private HashTagService hashTagService;
    @InjectMocks
    private HashTagController hashTagController;

    @Before
    public void setUp() {
        mvc = MockMvcBuilders.standaloneSetup(hashTagController)
                .setControllerAdvice(new BeachErrorHandler())
                .build();
    }


    @Test()
    public void hashtagException() throws Exception {
        mvc.perform(get("/hashtag")
                .contentType("application/json"))
                .andExpect(status().isOk());

    }

    @Test
    public void allhashtag() throws Exception {
        mvc.perform(get("/all/hashtag")
                .contentType("application/json"))
                .andExpect(status().isOk());

    }
}