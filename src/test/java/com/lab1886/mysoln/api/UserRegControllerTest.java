package com.lab1886.mysoln.api;

import com.lab1886.mysoln.MysolnApplication;
import com.lab1886.mysoln.exception.BeachErrorHandler;
import com.lab1886.mysoln.service.RegistrationService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(MysolnApplication.class)
public class UserRegControllerTest {
    private MockMvc mvc;


    @Mock
    private RegistrationService registrationService;
    @InjectMocks
    private UserRegController userRegController;

    @Before
    public void setUp() {
        mvc = MockMvcBuilders.standaloneSetup(userRegController)
                .setControllerAdvice(new BeachErrorHandler())
                .build();
    }


    @Test
    public void saveUser() throws Exception {
        String json = "{\n" +
                "  \"firstName\": \"sneha\",\n" +
                "  \"lastName\": \"vish\",\n" +
                "  \"password\": \"vish\",\n" +
                "  \"username\": \"vish\"\n" +
                "}";
        mvc.perform(post("/user/registration")
                .content(json).contentType("application/json"))
                .andExpect(status().isOk());

    }

    @Test
    public void saveAdminUser() throws Exception {
        String json = "{\n" +
                "  \"firstName\": \"sneha\",\n" +
                "  \"lastName\": \"vish\",\n" +
                "  \"password\": \"vish\",\n" +
                "  \"username\": \"vish\"\n" +
                "}";
        mvc.perform(post("/user/registration/admin")
                .content(json).contentType("application/json"))
                .andExpect(status().isOk());

    }

    @Test
    public void delete() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/user/registration")
                .contentType("application/json")
                .param("username", "hgfhgf"))
                .andExpect(status().isOk());

    }
}