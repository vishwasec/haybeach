package com.lab1886.mysoln.persistance.repository;

import com.lab1886.mysoln.persistance.Role;
import com.lab1886.mysoln.persistance.RoleTypes;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest

public class RoleRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private RoleRepository roleRepository;

    @Test
    public void findRoleByRoleName() {
        Optional<Role> actual = roleRepository.findRoleByRoleName(RoleTypes.AD);
        Assert.assertNotNull(actual);
        assertEquals(RoleTypes.AD, actual.get().getRoleName());
    }

}