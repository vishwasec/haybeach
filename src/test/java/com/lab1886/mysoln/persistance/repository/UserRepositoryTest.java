package com.lab1886.mysoln.persistance.repository;

import com.lab1886.mysoln.persistance.User;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository repository;

    @Test
    public void findByUsername() {
        User user = mockUser();
        entityManager.persist(user);
        User user1 = repository.findByUsername("vish");
        Assert.assertNotNull(user1);
        Assert.assertEquals("gopala", user1.getLastName());
        Assert.assertEquals("vishwas", user1.getFirstName());
        Assert.assertEquals("asdfa", user1.getPassword());
    }

    @Test
    public void findByUsernameNegative() {
        User user = mockUser();
        entityManager.persist(user);
        User user1 = repository.findByUsername("vish1");
        Assert.assertNull(user1);
    }

    private User mockUser() {
        User user = new User();
        user.setUsername("vish");
        user.setPassword("asdfa");
        user.setLastName("gopala");
        user.setFirstName("vishwas");
        return user;
    }
}